## Node Simple API Clean Architecture
###### with express & sqlite3

Requirement
1. node.js v12
2. internet

- How To run
```
> npm install
- Copy .env.template to .env (edit if necessary)
- npm start
- login admin with username: 'admin' password: (on .env)
```