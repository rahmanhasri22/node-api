const buildLogin = require('../use-cases/login');
const buildUserActions = require('./actions/user.action');

module.exports = (dependencies) => {
  const { db, jwt, bcrypt } = dependencies;
  const loginService = buildLogin({
    actions: buildUserActions({ db }),
    bcrypt,
    jwt, });

  const userLogin = async (httpRequest) => {
    const { body } = httpRequest;

    const login = await loginService(body);
    let statusCode = 201;
    if (login.status) {
      statusCode = 400;
    }

    return {
      statusCode,
      body: login,
    }
  };

  const checkUser = (httpRequest) => {
    const { headers } = httpRequest;
    let token = headers.authorization;
    if (!token) {
      return {
        statusCode: 401,
        status: 1,
        message: 'Authorization required',
      };
    }


    token = token.split('Bearer ')[1];
    return new Promise((resolve) => {
      jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, function (err, decoded) {
        if (err) {
          console.error(err);
          return resolve({
            statusCode: 401,
            status: 1,
            message: 'Token Invalid',
          });
        }

        httpRequest.decoded = decoded;
        return resolve({ status: 0 });
      });
    });
  };

  const checkAdmin = (httpRequest) => {
    const { decoded = {} } = httpRequest;
    if (!decoded.user || decoded.user.role !== 'ADMIN') {
      return {
        statusCode: 403,
        status: 1,
        message: 'Forbidden',
      };
    }

    return { status: 0 };
  };

  return {
    userLogin,
    checkUser,
    checkAdmin,
  };
};