const buildFindUsers = require('../use-cases/find-user');
const buildAddUser = require('../use-cases/add-user');
const buildUpdateUserPhoto = require('../use-cases/update-user-photo');
const buildUserActions = require('./actions/user.action');

module.exports = (dependencies = {}) => {
  const { db, bcrypt, fs } = dependencies;
  const userActions = buildUserActions({ db });
  const findUsers = buildFindUsers({ actions: userActions });
  const addUser = buildAddUser({ actions: userActions, bcrypt });
  const updateUserPhoto = buildUpdateUserPhoto({ actions: userActions });

  const getUsers = (httpRequest) => {
    const { query } = httpRequest;
    return findUsers(query)
      .then(rows => {
        return {
          statusCode: 200,
          body: {
            status: 0,
            message: 'Success',
            data: rows,
          },
        };
      });
  };

  const postUser = async (httpRequest) => {
    const { body } = httpRequest;
    if (!body.userId || !body.password) {
      return {
        statusCode: 400,
        body: {
          status: 1,
          message: 'userId and password cannot be null',
        },
      };
    }

    if (body.role !== 'STAFF' & body.role !== 'ADMIN') {
      return {
        statusCode: 400,
        body: {
          status: 1,
          message: 'Invalid role',
        },
      };
    }

    const userData = {
      userId: body.userId,
      password: body.password,
      fullName: body.fullName,
      role: body.role,
    };

    const insert = await addUser(userData);
    if (insert.status) {
      return {
        statusCode: 400,
        body: insert,
      };
    }

    return {
      statusCode: 201,
      body: insert,
    };
  };

  const updatePhoto = async (httpRequest) => {
    const { file, decoded = {} } = httpRequest;

    if (!file) {
      return {
        statusCode: 400,
        body: {
          status: 1,
          message: 'file is required',
        },
      };
    }

    const { user = {} } = decoded;
    await updateUserPhoto(user.userId, file.path);

    return {
      statusCode: 201,
      body: {
        status: 0,
        message: 'Upload successfully'
      },
    };
  };

  const getPhotoByUserId = async (httpRequest, httpResponse) => {
    const { params } = httpRequest;
    const user = await userActions.getByUserId(params.id);
    if (!user) {
      return httpResponse.status(200).json(user);
    }

    const readStream = fs.createReadStream(user.photo);
    readStream.pipe(httpResponse)

    return { done: true };
  };

  return {
    getUsers,
    postUser,
    updatePhoto,
    getPhotoByUserId,
  };
};
