module.exports = ({ db }) => {
  return {
    insertUser(data) {
      return new Promise((resolve, reject) => {
        let query = `
        INSERT INTO user
        (userId, fullName, password, role)
        VALUES(?, ?, ?, ?)
      `;

        db.run(query, [data.userId, data.fullName, data.password, data.role], function (err) {
          if(err) {
            return reject(err);
          }
          return resolve(true);
        });
      });
    },
    findUser(filter = {}) {
      return new Promise((resolve, reject) => {
        let query = `
        SELECT * FROM user
        WHERE 1=1 `;
        for (let key in filter) {
          query += `AND ${key} = '${filter[key]}'`;
        }

        db.all(query, function (err, rows) {
          if (err) {
            return reject(err);
          }
          return resolve(rows);
        });
      });
    },
    getByUserId(userId = '') {
      return new Promise((resolve, reject) => {{
        let query = `
        SELECT * FROM user
        WHERE userId = '${userId}'
      `;

        db.get(query, function (err, row) {
          if (err) {
            return reject(err);
          }
          return resolve(row);
        });
      }});
    },
    updatePhoto(userId = '', photo = '') {
      return new Promise((resolve, reject) => {
        let query = `
        UPDATE user
        SET photo = '${photo}'
        WHERE userId = '${userId}'
      `;

        db.run(query, function(err) {
          if (err) {
            return reject(err);
          }

          return resolve(this);
        });
      });
    },
  }
};