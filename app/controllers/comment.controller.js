const buildFetchComment = require('../use-cases/fetch-comment');

module.exports = (dependencies) => {
  const { request } = dependencies;
  const fetchComment = buildFetchComment({ request });

  const getComments = async (httpRequest) => {
    const { query } = httpRequest;
    const response = {
      statusCode: 200,
      body: null,
    };
    if (Number(query.id)) {
      response.body = await fetchComment(query.id);
    }

    return response;
  };

  return {
    getComments,
  };
};
