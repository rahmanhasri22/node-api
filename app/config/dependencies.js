const fs = require('fs');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const request = require('node-fetch');

const db = require('../drivers/database');
const upload = require('../drivers/multer');

module.exports = {
  db,
  jwt,
  bcrypt,
  upload,
  fs,
  request,
};
