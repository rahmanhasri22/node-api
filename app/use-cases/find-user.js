const User = require('../entities/User');

module.exports = ({ actions }) => {
  return (query = {}) => {
    const queryExtended = {};
    const userColumns = ['userId', 'fullName', 'role'];
    for (let key in query) {
      if (query[key] && userColumns.includes(key)) {
        queryExtended[key] = query[key];
      }
    }

    return actions.findUser(queryExtended)
      .then((data = []) => {
        return data.map(row => new User({
          userId: row.userId,
          fullName: row.fullName,
          role: row.role,
          photo: row.photo,
        }));
      });
  };
};
