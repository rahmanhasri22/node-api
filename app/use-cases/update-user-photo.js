module.exports = ({ actions }) => {
  return (userId, filePath) => {
    return actions.updatePhoto(userId, filePath);
  };
};
