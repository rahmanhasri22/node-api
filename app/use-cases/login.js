const User = require('../entities/User');

module.exports = ({ actions, bcrypt, jwt }) => {
  return async (input = {}) => {
    const { userId, password } = input;
    let user = await actions.getByUserId(userId);
    const message = 'Invalid userId Or password';
    if (!user) {
      return {
        status: 1,
        message,
      };
    }

    const matchPassword = await bcrypt.compare(password, user.password);

    if (!matchPassword) {
      return {
        status: 1,
        message,
      };
    }

    user = new User(user);

    return {
      status: 0,
      message: 'Login success',
      fullName: user.fullName,
      role: user.role,
      accessToken: user.createAccessToken(jwt, process.env.ACCESS_TOKEN_SECRET),
      refreshToken: user.createRefreshToken(jwt, process.env.REFRESH_TOKEN_SECRET),
    };
  };
};
