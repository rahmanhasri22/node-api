const User = require('../entities/User');

module.exports = ({ actions, bcrypt }) => {
  return async (input) => {
    const userWithSameUserId = await actions.getByUserId(input.userId);
    if (userWithSameUserId) {
      return {
        status: 1,
        message: 'Duplicate UserId',
      };
    }

    const newUser = await actions.insertUser({
      ...input,
      password: bcrypt.hashSync(input.password, 10),
    })
      .then(() => new User(input));

    return {
      status: 0,
      message: 'User Created',
      data: newUser,
    };
  };
};
