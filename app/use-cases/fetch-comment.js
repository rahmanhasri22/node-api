const Comment = require('../entities/Comment');

module.exports = ({ request }) => {
  return async (postId) => {
    const response = await request(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`);

    return response.json()
      .then((data) => data.map((c, index) => new Comment({
        no: index + 1,
        postId: c.postId,
        id: c.id,
        name: c.name,
        email: c.email,
        body: c.body,
      },
    )));
  };
};
