module.exports = class Comment {
  constructor({
    no,
    postId,
    id,
    name,
    email,
    body,
  }) {
    this.no = no;
    this.postId = postId;
    this.id = id;
    this.name = name;
    this.email = email;
    this.body = body;
  }
}