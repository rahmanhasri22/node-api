module.exports = class User {
  constructor({
    userId,
    fullName,
    role,
    photo,
  }) {
    this.userId = userId;
    this.fullName = fullName;
    this.role = role;
    this.photo = photo;
  }

  createAccessToken(jwt, secret) {
    return jwt.sign({
      user: {
        userId: this.userId,
        role: this.role,
      },
    }, secret, {
      expiresIn: '10h',
    });
  }

  createRefreshToken(jwt, secret) {
    return jwt.sign({
      user: {
        userId: this.userId,
        role: this.role,
      },
    }, secret, {
      expiresIn: '6h',
    });
  }
}
