const express = require('express')
const path = require('path');
require('dotenv').config();

const dependencies = require('./config/dependencies');

const buildAuthController = require('./controllers/auth.controller');
const buildUserController = require('./controllers/user.controller')
const buildCommentController = require('./controllers/comment.controller');

const callbackResponse = require('./drivers/express-callback/response');
const callbackMiddleware = require('./drivers/express-callback/middleware');

const authController = buildAuthController(dependencies);
const userController = buildUserController(dependencies);
const commentController = buildCommentController(dependencies);

const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use('/uploads', express.static(path.join(process.env.PWD, 'uploads')))

app.post('/api/user/login', callbackResponse(authController.userLogin));

app.use(callbackMiddleware(authController.checkUser));

app.get('/api/user', callbackResponse(userController.getUsers));
app.post('/api/user', callbackMiddleware(authController.checkAdmin), callbackResponse(userController.postUser));
app.post('/api/user/photo', dependencies.upload.single('file'), callbackResponse(userController.updatePhoto));
app.get('/api/user/photo/:userId', callbackResponse(userController.getPhotoByUserId));
app.get('/api/comments', callbackResponse(commentController.getComments));

module.exports = app;
