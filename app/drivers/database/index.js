const bcrypt = require('bcrypt');
const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database('./user.db');

const init = () => {
  db.serialize(() => {
    const queryCreateUserTable = `CREATE TABLE IF NOT EXISTS user
      (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        userId VARCHAR(255) NOT NULL UNIQUE,
        password VARCHAR(255) NOT NULL,
        role VARCHAR(10) CHECK( role IN ('ADMIN', 'STAFF') ) NOT NULL DEFAULT 'STAFF',
        fullName VARCHAR(255),
        photo TEXT
      )`;

    db.run(queryCreateUserTable, function (err) {
      if (err) {
        console.log(err);
        throw '[ERR]';
      }
      console.log('SQLITE table user created');
    });

    const hashPassword = bcrypt.hashSync(process.env.DEFAULT_ADMIN_PASS, 10);
    const queryCreateAdmin = `
      INSERT OR IGNORE INTO user
      (userId, password, role, fullName)
      VALUES ('admin', '${hashPassword}', 'ADMIN', 'this is admin')
    `;

    db.run(queryCreateAdmin, function (err) {
      if (err) {
        return console.log(err);
      }
      return console.log('Admin created');
    })
  });
};

init();

module.exports = db;
