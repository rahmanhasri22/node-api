module.exports = (controller) => {
  return async (req, res) => {
    try {
      const httpResponse = await controller(req, res);
      if (httpResponse.done) {
        return;
      }
      return res.status(httpResponse.statusCode).json(httpResponse.body);
    } catch (error) {
      console.log(error);
      return res.status(500).send({ error: 'Internal Server Error.' });
    }
  }
};