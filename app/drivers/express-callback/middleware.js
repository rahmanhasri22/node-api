module.exports = (controller) => {
  return async (req, res, next) => {
    try {
      const result = await controller(req);
      if (result.status) {
        return res.status(result.statusCode).json({ ...result });
      }
      return next();
    } catch (error) {
      console.log(error);
      return res.status(500).send({ error: 'Internal Server Error.' });
    }
  }
};
